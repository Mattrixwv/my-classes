//Games/Solitaire/HEaders/TermColors.hpp
//Matthew Ellison
// Created: 3-6-18
//Modified: 3-6-18
//This file contains the class that helps control the colors presented on the terminal
//It will also aid in printing all of the cards and stacks of cards
//This class uses the ANSI escape sequences to color the terminal
///This class still has bugs in it
/*
	Copyright (C) 2018  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef TERMCOLORS_HPP
#define TERMCOLORS_HPP

#include <string>
#include <sstream>

namespace mee{


enum class textAttributes {ALL_OFF, BOLD, UNDERSCORE = 4, BLINK};
enum class textColors {BLACK = 30, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE};
enum class backColors {BLACK = 40, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE};


class TermColors{
private:
public:
	TermColors()
	{		}
	std::string eraseLine()
	{	return "\033[K";	}
	std::string eraseLine(unsigned int row){
		std::stringstream text;
		text << "\033[" << row << ";0H\033[2J";
		return text.str();
	}
	std::string eraseScreen()
	{	return "\033[2J";	}
	std::string setAttr(mee::textAttributes atr){

	}
	std::string setText(mee::textColors txt){
		std::stringstream text;
		text << "\033[" << static_cast<int>(txt) << 'm';
		return text.str();
	}
	std::string setBackground(mee::backColors bck){
		std::stringstream text;
		text << "\033[" << static_cast<int>(bck) << 'm';
		return text.str();
	}
	std::string setColor(mee::textColors txt, mee::backColors bck){
		std::stringstream text;
		text << "\033[" << static_cast<int>(txt) << ';' << static_cast<int>(bck) << 'm';
		return text.str();
	}
	std::string setPos(unsigned int row, unsigned int col = 0){
		std::stringstream text;
		text << "\033[" << row << ';' << col << 'H';
		return text.str();
	}
};	//End TermColors

}	//End namespace mee

#endif //TERMCOLORS_HPP
