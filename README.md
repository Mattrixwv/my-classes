#This is a repository of classes and functions that are not built in but I have found it useful to keep around

All of these are contained in the namespace mee. Make sure to use it when trying to reference functions or classes.

#Algorithms.hpp
This file is a collection of different functions that I have found myself reusing for various reasons.
I built many of these functions as templates and have not seen a reason to change that and turn it into a library. But that could happen at some point in the future.

getPrimes(goalNumber) is a function that returns a vector of all the prime number <= the parameter you pass into it
getNumPrimes(numberOfPrimes) is a function that returns a vector of a precise number of prime numbers. This number is determined by the parameter you pass to it. This is a function that you need to be careful with because you can pass a number so large that it causes an overflow. If an overflow happens it will stop and return what it was able to find. Because it is a template it should work with various bigint libraries, as long as they support the basic math operators.
getFactors(goalNumber) is a function that returns a vector of all the prime factors of parameter you pass it.
getDivisors(num) is a function that returns a vector of all the divisors of the number you pass to it as a parameter.
getSum(numbers) is a function that takes a vector as an argument and returns the sum of all of the elements of that vector.
isFound(num, list) is a function that takes an object and a vector and returns true if the object is found as an element in the vector.
getPermutations(master, num) is a function that is meant to take a string when used normally and will return all permutations of that string. It has a second parameter that is mostly used for recursion, but can be used if you want to skip a certain number of characters on the front of the string.
getFib(num) is a function that returns Fibonacci(num)
getMpzFib(num) is a function that returns Fibonacci(num) in an mpz_class. mpz_class is the bigint class of the gmp library. In order to use this function you need to use the flag -DNEEDGMP into the compiler as well as -lgmpxx -lgmp into the linker. It also requires that you have the development version of the gmp library.
bubbleSort(nums) is a function that takes a vector as a parameter and uses a bubblesort method to sort all of the elements.
quickSort(nums) is a function that takes a vector as a parameter and uses a quicksort method to sort all of the elements.
search(nums, num) is a function that takes a vector and an object and returns the location in the vector of the of the element. If the element is not found it returns a -1.
findMin(arr) is a function that returns the smallest element in a vector.
findMax(arr) is a function that returns the largest element in a vector.


#Dice.hpp
This is a class that acts as a simple dice.
It is basically a random number generator with limits on its output numbers.

For the constructor it takes a single unsigned integer to represent the number of sides the die has. If no number is given the default is 6 to simulate a classic die.
getFace() returns the currently "face up" side of the dice.
getSides() returns the number of sides the die has.
roll() uses the random number generator used in the class to make another roll and returns the result.


#Stopwatch.hpp
This is a class that acts as a simple timer.
I use it mostly to time algorithms and see how long it takes to run or see if there is a difference between two different algorithms.
It is so simple that I currently just include it in files rather than using it as a library.

start() creates the first time stamp that will later be used to calculate the amount of time that has passed. This function should be called before stop is called.
stop() creates the second time stamp that will later be used to calculate the amount of time that has passed. This function should not be called before start has been called.
getTime() acts slightly differently based on what functions have been called. If neither start() nor stop() has been called then it will return -1. If start() has been called, but not stop() it will return the difference in time between when start was called and now. If both start and stop were called it will return the differenct in time between the time those two functions were called. Time will be returned in the default resolution of std::chrono::high_resolution_clock (currently nanoseconds).
getNano(), getMicro(), getMilli(), getSeconds(), getMinutes(), getHours(). All of these function work in the same way as getTime, but returns the time in a specific resolution. (nanoseconds, microseconds, milliseconds, seconds, minutes, hours)
getStr() returns a string with the "best" time resolution. It looks for a XXX.XXX <TimeResolution> format. It switches to minutes after 120 seconds and hours after 120 minutes
reset() resets all variables in the class back to their defaults. This function is meant to be called if you want to use the same timer multiple times in a program. It shouldn't be necessary to call, but it is still safer and you are less likely to run in to any kind of bugs.
Usage exampe:
	mee::Stopwatch timer;
	timer.start();
	//Your timed code here
	timer.stop();
	std::cout << "It took " << timer.getMilli() << " milliseconds to run this algorithm";
	//Or more simply if you don't know the run time or are afraid there might be different run times on different machines
	std::cout << "It took " << timer.getStr() << " to run this algorithm";


#TermColors
This is a library that changes the color of output on most modern terminals.
It is currently not working correctly.