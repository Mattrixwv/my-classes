NUMCORES = $(shell grep -c "^processor" /proc/cpuinfo)
NUMCORESWIN = ${NUMBER_OF_PROCESSORS}
TESTFLAGS = -O2 -std=c++20 -Wall -fcoroutines
LINKEDLIBS = -lgmp -lgmpxx
INCLUDE_DIR = ./headers
TESTDIR = ./test/mee
BINDIR = ./bin
LOGDIR = ./logs


all: testMulti
test: directory testAlgorithms testStopwatch testDice
testAlgorithms: testNumberAlgorithms testStringAlgorithms testVectorAlgorithms
testMulti: directory
	$(MAKE) testAlgorithms testStopwatch testDice -j $(NUMCORESWIN)
	@echo 'All tests completed successfully'


#Non-build jobs
directory:
	mkdir -p $(BINDIR) $(LOGDIR)

#Algorithms
test%Algorithms: $(TESTDIR)/test%Algorithms.cpp
	$(CXX) $(TESTFLAGS) -o $(BINDIR)/$@.exe $< -I $(INCLUDE_DIR) $(LINKEDLIBS)
	$(BINDIR)/$@.exe > $(LOGDIR)/$@.txt

#Stopwatch
testStopwatch: $(TESTDIR)/testStopwatch.cpp
	$(CXX) $(TESTFLAGS) -o $(BINDIR)/$@.exe $< -I $(INCLUDE_DIR) $(LINKEDLIBS)
	$(BINDIR)/$@.exe > $(LOGDIR)/$@.txt

#Dice
testDice: $(TESTDIR)/testDice.cpp
	$(CXX) $(TESTFLAGS) -o $(BINDIR)/$@.exe $< -I $(INCLUDE_DIR) $(LINKEDLIBS)
	$(BINDIR)/$@.exe > $(LOGDIR)/$@.txt

#Clean up/Remove all files and folders created
.PHONY: clean

clean:
	rm -f $(BINDIR)/* $(LOGDIR)/*
	rmdir $(BINDIR) $(LOGDIR)
