//myClasses/headers/mee/vectorAlgorithms.hpp
//Matthew Ellison
// Created: 07-02-21
//Modified: 07-02-21
//This file contains declarations of functions I have created to manipulate vectors and the data inside them
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef MEE_VECTOR_ALGORITHMS_HPP
#define MEE_VECTOR_ALGORITHMS_HPP


#include <cinttypes>
#include <sstream>
#include <vector>


namespace mee{


//This is a function that returns the sum of all elements in a vector
template <class T>
T getSum(const std::vector<T>& ary){
	T sum = 0;
	for(unsigned int cnt = 0;cnt < ary.size();++cnt){
		sum += ary.at(cnt);
	}
	return sum;
}

//This is a function that returns the product of all elmements in a vector
template <class T>
T getProduct(const std::vector<T>& ary){
	//Make sure there is something in the array
	if(ary.size() == 0){
		return 0;
	}

	//Multiply all elements in the array together
	T prod = 1;
	for(T cnt = 0;cnt < ary.size();++cnt){
		prod *= ary.at(cnt);
	}
	return prod;
}

//This is a function that searches a vecter for an element. Returns true if they key is found in list
template <class T>
bool isFound(std::vector<T> ary, T key){
	typename std::vector<T>::iterator location = std::find(ary.begin(), ary.end(), key);
	if(location == ary.end()){
		return false;
	}
	else{
		return true;
	}
}

//This is a function that performs a bubble sort on a vector
template <class T>
void bubbleSort(std::vector<T>& ary){
	bool notFinished = true;	//A flag to determine if the loop is finished
	for(int numLoops = 0;numLoops < ary.size();++numLoops){	//Loop until you finish
		notFinished = false;	//Assume you are finished until you find an element out of order	
		//Loop through every element in the vector, moving the largest one to the end
		for(int cnt = 1;cnt < (ary.size() - numLoops);++cnt){	//use size - 1 to make sure you don't go out of bounds
			if(ary.at(cnt) < ary.at(cnt - 1)){
				std::swap(ary.at(cnt), ary.at(cnt - 1));
				notFinished = true;
			}
		}
	}
}

//This is a helper function for quickSort. It chooses a pivot element and sorts everything to larger or smaller than the pivot. Returns location of pivot
template <class T>
int64_t partition(std::vector<T>& ary, int64_t bottom, int64_t top){
	int64_t pivot = ary.at(top);	//Pick a pivot element
	int64_t smaller = bottom - 1;	//Keep track of where all elements are smaller than the pivot

	//Loop through every element in the vector testing if it is smaller than pivot
	for(int64_t cnt = bottom;cnt < top;++cnt){
		//If the element is smaller than pivot move it to the correct location
		if(ary.at(cnt) < pivot){
			//Increment the tracker for elements smaller than pivot
			++smaller;
			//Swap the current element to the correct location for being smaller than the pivot
			std::swap(ary.at(smaller), ary.at(cnt));
		}
	}

	//Move the pivot element to the correct location
	++smaller;
	std::swap(ary.at(top), ary.at(smaller));

	//Return the pivot element
	return smaller;
}
//This is the function that actually performs the quick sort on the vector
template <class T>
void quickSort(std::vector<T>& ary, int64_t bottom, int64_t top){
	//Make sure you have a valid slice of the vector
	if(bottom < top){
		//Get the pivot location
		int64_t pivot = partition(ary, bottom, top);

		//Sort all element less than the pivot
		quickSort(ary, bottom, pivot - 1);
		//Sort all element greater than the pivot
		quickSort(ary, pivot + 1, top);
	}
}
//This is a function that makes quick sort easier to start
template <class T>
void quickSort(std::vector<T>& ary){
	//Call the other quickSort function with all the necessary info
	quickSort(ary, 0, ary.size() - 1);
}

//This is a function that performs a search on a vector and returns the subscript of the item being searched for
template <class T>
int64_t search(const std::vector<T>& ary, T num){
	int64_t subscript = 0;	//Start with the subscript at 0
	//Step through every element in the vector and return the subscript if you find the correct element
	while(subscript < ary.size()){
		if(ary.at(subscript) == num){
			return subscript;
		}
		else{
			++subscript;
		}
	}
	//If you cannot find the element return -1
	return -1;
}

//This function finds the smallest element in a vector
template <class T>
T findMin(const std::vector<T>& ary){
	T min;	//For the smallest element

	//Make sure the vector is not empty
	if(ary.size() > 0){
		//Use the first element as the smallest element
		min = ary.at(0);
		//Run through every element in the vector, checking it against the current minimum
		for(int cnt = 1;cnt < ary.size();++cnt){
			//If the current element is smaller than the minimum, make it the new minimum
			if(ary.at(cnt) < min){
				min = ary.at(cnt);
			}
		}
	}

	//Return the element
	return min;
}

//This function finds the largest element in a vector
template <class T>
T findMax(const std::vector<T>& ary){
	T max;	//For the largest element

	//Make sure the vector is not empty
	if(ary.size() > 0){
		//Use the first element as the largest element
		max = ary.at(0);
		//Run through every element in the vector, checking it against the current minimum
		for(int cnt = 1;cnt < ary.size();++cnt){
			//If the current element is larger than the maximum, make it the new maximum
			if(ary.at(cnt) > max){
				max = ary.at(cnt);
			}
		}
	}

	//Return the element
	return max;
}

//Print a vector
template <class T>
std::string printVector(std::vector<T>& ary){
	std::stringstream str;
	str << "[";
	for(int cnt = 0;cnt < ary.size();++cnt){
		str << ary[cnt];
		if(cnt < ary.size() - 1){
			str << ", ";
		}
	}
	str << "]";
	return str.str();
}


}


#endif	//MEE_VECTOR_ALGORITHMS_HPP
