//myClasses/headers/mee/Dice.hpp
//Matthew Ellison
// Created: 01-26-19
//Modified: 07-02-21
//This is a simple class to simulate a dice for games
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef MEE_DICE_HPP
#define MEE_DICE_HPP


#include <random>


namespace mee{

template<class T>
class Dice{
private:
	T face;		//Holds the currently rolled number
	T sides;	//Holds the number of sides the dice has
	std::default_random_engine generator;	//The number generator that all the numbers come from
	std::uniform_int_distribution<T> dist;	//A distribution to make sure the numbers come out relatively evenly
public:
	Dice(T sides = 6) : face(1), sides(sides), generator(std::random_device{}()), dist(1, sides){
	}
	//Setup ways to get information from the class
	T getFace() const{
		return face;
	}
	T getSides() const{
		return sides;
	}
	//Used to simulate rolling the dice. Returns the new number
	T roll(){
		face = dist(generator);
		return face;
	}
};


}	//namespace mee


#endif	//MEE_DICE_HPP
