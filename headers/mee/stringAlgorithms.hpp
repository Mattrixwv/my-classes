//myClasses/headers/mee/stringAlgorithms.hpp
//Matthew Ellison
// Created: 07-02-21
//Modified: 07-02-21
//This file contains declarations of functions I have created to manipulate strings
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef MEE_STRING_ALGORITHMS_HPP
#define MEE_STRING_ALGORITHMS_HPP


#include <string>
#include <vector>


namespace mee{


//This is a function that creates all permutations of a string and returns a vector of those permutations.
std::vector<std::string> getPermutations(std::string master, int num = 0){
	std::vector<std::string> perms;
	//Check if the number is out of bounds
	if((num >= master.size()) || (num < 0)){
		return perms;
	}
	//If this is the last possible recurse just return the current string
	else if(num == (master.size() - 1)){
		perms.push_back(master);
		return perms;
	}
	//If there are more possible recurses, recurse with the current permutation
	std::vector<std::string> temp;
	temp = getPermutations(master, num + 1);
	perms.insert(perms.end(), temp.begin(), temp.end());
	//You need to swap the current letter with every possible letter after it
	//The ones needed to swap before will happen automatically when the function recurses
	for(int cnt = 1;(num + cnt) < master.size();++cnt){
		std::swap(master[num], master[num + cnt]);
		temp = getPermutations(master, num + 1);
		perms.insert(perms.end(), temp.begin(), temp.end());
		std::swap(master[num], master[num + cnt]);
	}

	//The array is not necessarily in alpha-numeric order. So if this is the full array sort it before returning
	if(num == 0){
		std::sort(perms.begin(), perms.end());
	}
	return perms;
}

//This function returns the number of times the character occurs in the string
int findNumOccurrence(std::string str, char ch){
	int num = 0;	//Set the number of occurrences to 0 to start
	//Loop through every character in the string and compare it to the character passed in
	for(char strCh : str){
		//If the character is the same as the one passed in increment the counter
		if(strCh == ch){
			++num;
		}
	}
	//Return the number of times the character appeared in the string
	return num;
}

//Return a vector of strings split on the delimiter
std::vector<std::string> split(std::string str, char delimiter){
	std::vector<std::string> splitStrings;
	int location = 0;
	location = str.find(delimiter);
	while(location != std::string::npos){
		//Split the string
		std::string firstString = str.substr(0, location);
		str = str.substr(location + 1);	//+1 to skip the delimiter itself
		//Add the string to the vector
		splitStrings.push_back(firstString);
		//Get the location of the next delimiter
		location = str.find(delimiter);
	}
	//Get the final string if it isn't empty
	if(!str.empty()){
		splitStrings.push_back(str);
	}
	//Return the vector of strings
	return splitStrings;
}

//This function returns true if the string passed in is a palindrome
bool isPalindrome(std::string str){
	std::string rev = str;
	std::reverse(rev.begin(), rev.end());
	if(str == rev){
		return true;
	}
	else{
		return false;
	}
}


}


#endif	//MEE_STRING_ALGORITHMS_HPP
