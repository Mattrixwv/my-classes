//myClasses/test/mee/testNumberAlgorithms.cpp
//Matthew Ellison
// Created: 07-02-21
//Modified: 07-02-21
//This file contains tests for the functions in my numberAlgorithms library
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <iostream>
#include <gmpxx.h>
#include "mee/numberAlgorithms.hpp"
#include "mee/Stopwatch.hpp"


//Defines a type for functions so there can be an array of functions
typedef bool (*boolFn)();


//Prototypes for test functions
bool testIsPrime();
bool testGetPrimes();
bool testGetNumPrimes();
bool testGetFactors();
bool testGetDivisors();
bool testGetFib();
bool testGetAllFib();
bool testToBin();
bool testFactorial();
bool testSieveOfEratosthenes();


int main(){
	mee::Stopwatch timer;
	bool passedTest = false;
	bool allPassed = true;
	std::vector<boolFn> functions { testIsPrime, testGetPrimes, testGetNumPrimes, testGetFactors, testGetDivisors, testGetFib,
			testGetAllFib, testToBin, testFactorial, testSieveOfEratosthenes };
	std::vector<std::string> names { "isPrime", "getPrimes", "getNumPrimes", "getFactors", "getDivisors", "getFib",
			"getAllFib", "toBin", "factorial", "sieveOfEratosthenes" };

	//Start doing tests and print out the results of each
	for(size_t cnt = 0;cnt < functions.size();++cnt){
		timer.start();
		passedTest = functions[cnt]();
		timer.stop();
		if(passedTest){
			std::cout << "Function " << names[cnt] << "() passed the test\n";
		}
		else{
			std::cout << "Function " << names[cnt] << "() failed the test\n";
			allPassed = false;
		}
		std::cout << "The test took " << timer.getStr() << "\n\n" << std::endl;
	}

	if(allPassed){
		return 0;
	}
	else{
		return 1;
	}
}


bool testIsPrime(){
	//Test 1
	int64_t num = 2;
	bool correctAnswer = true;
	bool answer = mee::isPrime(num);
	if(correctAnswer != answer){
		return false;
	}
	//Test 2
	num = 97;
	correctAnswer = true;
	answer = mee::isPrime(num);
	if(correctAnswer != answer){
		return false;
	}
	//Test 3
	num = 1000;
	correctAnswer = false;
	answer = mee::isPrime(num);
	if(correctAnswer != answer){
		return false;
	}
	//Test 4
	num = 1;
	correctAnswer = false;
	answer = mee::isPrime(num);
	if(correctAnswer != answer){
		return false;
	}

	//If the false was not triggered it must have passed all tests
	return true;
}

bool testGetPrimes(){
	std::vector<uint64_t> correctAnswer {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
	uint64_t topNum = 100;
	std::vector<uint64_t> answer = mee::getPrimes(topNum);
	if(correctAnswer != answer){
		return false;
	}

	//If the false was not triggered it must have passed all tests
	return true;
}

bool testGetNumPrimes(){
	std::vector<uint64_t> correctAnswer {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
	uint64_t numPrimes = 25;
	std::vector<uint64_t> answer = mee::getNumPrimes(numPrimes);
	if(correctAnswer != answer){
		return false;
	}

	//If the false was not triggered it must have passed all tests
	return true;
}

bool testGetFactors(){
	std::vector<uint64_t> correctAnswer {2, 2, 5, 5};
	uint64_t number = 100;
	std::vector<uint64_t> answer = mee::getFactors(number);
	if(correctAnswer != answer){
		return false;
	}

	correctAnswer = {2, 7, 7};
	number = 98;
	answer = mee::getFactors(number);
	if(correctAnswer != answer){
		return false;
	}

	//If a false was not triggered it must have passed all tests
	return true;
}

bool testGetDivisors(){
	std::vector<uint64_t> correctAnswer {1, 2, 4, 5, 10, 20, 25, 50, 100};
	uint64_t topNum = 100;
	std::vector<uint64_t> answer = mee::getDivisors(topNum);
	if(correctAnswer != answer){
		return false;
	}

	//If the false was not triggered it must have passed all tests
	return true;
}

bool testGetFib(){
	//Test the imbeded type getFib function
	uint64_t correctAnswer = 144;
	uint64_t number = 12;
	uint64_t answer = mee::getFib(number);
	if(correctAnswer != answer){
		std::cout << "getFit() failed at test 1" << std::endl;
		return false;
	}

	number = 20;
	correctAnswer = 6765;
	answer = mee::getFib(number);
	if(correctAnswer != answer){
		std::cout << "getFit() failed at test 2" << std::endl;
		return false;
	}


	//Test the gmp integer function
	mpz_class mpzNumber = 12;
	mpz_class longCorrectAnswer = 144;
	mpz_class longAnswer = mee::getFib(mpzNumber);
	if(longCorrectAnswer != longAnswer){
		std::cout << "getFib() for mpz failed at test 3" << std::endl;
		return false;
	}

	mpzNumber = 4782;
	longCorrectAnswer = "1070066266382758936764980584457396885083683896632151665013235203375314520604694040621889147582489792657804694888177591957484336466672569959512996030461262748092482186144069433051234774442750273781753087579391666192149259186759553966422837148943113074699503439547001985432609723067290192870526447243726117715821825548491120525013201478612965931381792235559657452039506137551467837543229119602129934048260706175397706847068202895486902666185435124521900369480641357447470911707619766945691070098024393439617474103736912503231365532164773697023167755051595173518460579954919410967778373229665796581646513903488154256310184224190259846088000110186255550245493937113651657039447629584714548523425950428582425306083544435428212611008992863795048006894330309773217834864543113205765659868456288616808718693835297350643986297640660000723562917905207051164077614812491885830945940566688339109350944456576357666151619317753792891661581327159616877487983821820492520348473874384736771934512787029218636250627816";
	longAnswer = mee::getFib(mpzNumber);
	if(longCorrectAnswer != longAnswer){
		std::cout << "getFib() for mpzfailed at test 4" << std::endl;
		return false;
	}


	//If the false was not triggered it must have passed all tests
	return true;
}

bool testGetAllFib(){
	std::vector<uint64_t> correctAnswer {1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89};
	uint64_t highestNumber = 100;
	std::vector<uint64_t> answer = mee::getAllFib(highestNumber);
	if(correctAnswer != answer){
		return false;
	}

	correctAnswer = {1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987};
	highestNumber = 1000;
	answer = mee::getAllFib(highestNumber);
	if(correctAnswer != answer){
		return false;
	}

	//If a false was not triggered it must have passed all tests
	return true;
}

bool testToBin(){
	//Test 1
	int num = 7;
	std::string correctAnswer = "111";
	std::string answer = mee::toBin(num);
	if(correctAnswer != answer){
		return false;
	}

	//Test 2
	num = 0;
	correctAnswer = "0";
	answer = mee::toBin(num);
	if(correctAnswer != answer){
		return false;
	}

	//Test 3
	num = 1000000;
	correctAnswer = "11110100001001000000";
	answer = mee::toBin(num);
	if(correctAnswer != answer){
		return false;
	}

	//Test 4
	uint64_t num2 = 8;
	correctAnswer = "1000";
	answer = mee::toBin(num2);
	if(correctAnswer != answer){
		return false;
	}

	//If it hasn't failed a test then return true for passing all the tests
	return true;
}

bool testFactorial(){
	//Test 1
	int num = 1;
	int correctAnswer = 1;
	int answer = mee::factorial(num);
	if(correctAnswer != answer){
		return false;
	}

	//Test 2
	num = 10;
	correctAnswer = 3628800;
	answer = mee::factorial(num);
	if(correctAnswer != answer){
		return false;
	}

	//Test 3
	num = -5;
	correctAnswer = 1;
	answer = mee::factorial(num);
	if(correctAnswer != answer){
		return false;
	}

	//If it hasn't failed a test then return true for passing all the tests
	return true;
}

bool testSieveOfEratosthenes(){
	//Test 1
	mee::Generator<int> gen = mee::sieveOfEratosthenes<int>();
	std::vector<int> correctAnswer{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
	std::vector<int> answer;
	for(int cnt = 0;cnt < 25;++cnt){
		int prime = gen.next();
		answer.push_back(prime);
	}
	if(correctAnswer != answer){
		return false;
	}

	//Test 2
	mee::Generator<mpz_class> mpzGen = mee::sieveOfEratosthenesAlt<mpz_class>();
	std::vector<mpz_class> bigCorrectAnswer{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
	std::vector<mpz_class> bigAnswer;
	for(int cnt = 0;cnt < 25;++cnt){
		mpz_class prime = mpzGen.next();
		bigAnswer.push_back(prime);
	}
	if(correctAnswer != answer){
		return false;
	}

	//If it hasn't failed a test then return true for passing all the tests
	return true;
}
