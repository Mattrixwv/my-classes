//myClasses/testDice.cpp
//Matthew Ellison
// Created: 1-26-19
//Modified: 1-26-19
//This file is a simple program to test the Dice class
/*
	Copyright (C) 2018  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <cinttypes>
#include "mee/Dice.hpp"

const uint64_t LENGTH_OF_TEST = 100;	//How many times the dice will get rolled * the number of sides


int main(){
	//Check the default constructor
	std::cout << "Checking the default constructor of the Dice class:\n";
	mee::Dice<int> die1;
	if(die1.getSides() == 6){
		std::cout << "Default constructor passes the test\n";
	}
	else{
		std::cout << "Default constructor has an incorrect number of sides\n";
	}

	//Check the constructor with a high side number
	std::cout << "\nChecking a constructor with a high number of sides\n";
	mee::Dice<uint64_t> die2(50);
	if(die2.getSides() == 50){
		std::cout << "Parameterized constructor passed the test\n";
	}
	else{
		std::cout << "Parameterized constructor has an incorrect number of sides\n";
	}

	//Run a long test to see how the rolls balance out for the default constructor
	std::cout << "\nStarting test for the default constructor die:\n";
	//Setup an array to track how many times a number has been rolled
	std::vector<unsigned int> die1Rolls;
	die1Rolls.reserve(die1.getSides());
	for(int cnt = 0;cnt < die1.getSides();++cnt){
		die1Rolls.push_back(0);
	}
	//A loop to roll the die and record the results
	for(uint64_t cnt = 0;cnt < (LENGTH_OF_TEST * die1.getSides());++cnt){
		uint64_t num = die1.roll();
		++die1Rolls[num - 1];	//num - 1 to account for dice starting at 1 and array starting at 0
	}
	//Print out the results
	for(size_t cnt = 0;cnt < die1Rolls.size();++cnt){
		std::cout << cnt + 1 << ". " << die1Rolls[cnt] << '\n';
	}

	//Run a long test to see how the rolls balance out for the parameterized constructor
	std::cout << "\nStarting test for the parameterized constructor die:\n";
	//Setup an array to track how many times a number has been rolled
	std::vector<unsigned int> die2Rolls;
	die2Rolls.reserve(die2.getSides());
	for(uint64_t cnt = 0;cnt < die2.getSides();++cnt){
		die2Rolls.push_back(0);
	}
	//A loop to roll the die and record the results
	for(uint64_t cnt = 0;cnt < (LENGTH_OF_TEST * die2.getSides());++cnt){
		int num = die2.roll();
		++die2Rolls[num - 1];	//num - 1 to account for dice starting at 1 and array starting at 0
	}
	//Print out the results
	for(uint64_t cnt = 0;cnt < die2Rolls.size();++cnt){
		std::cout << cnt + 1 << ". " << die2Rolls[cnt] << '\n';
	}

	std::cout << "Test of die class completed!" << std::endl;

	return 0;
}

/*Results:

*/
