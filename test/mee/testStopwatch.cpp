//myClasses/testStopwatch.cpp
//Matthew Ellison
// Created: 02-12-19
//Modified: 07-09-20
//This file is a simple test for the Stopwatch class


#include <iostream>
#include "mee/Stopwatch.hpp"

int main(){
	mee::Stopwatch timer;
	//Try to stop the timer without starting it
	std::cout << "Testing the stopBeforeStart error" << std::endl;
	try{
		timer.stop();
		std::cout << "Test failed" << std::endl;
	}
	catch(mee::Stopwatch::invalidTimeResolution){
		std::cout << "There was an invalid time resolution\n";
		std::cout << "Test failed" << std::endl;
	}
	catch(mee::Stopwatch::timeBeforeStart){
		std::cout << "Tried to get time before the stopwatch was started\n";
		std::cout << "Test failed" << std::endl;
	}
	catch(mee::Stopwatch::stopBeforeStart){
		std::cout << "Tried to stop the stopwatch before it was started\n";
		std::cout << "Test successful" << std::endl;
	}

	//Try to get the time on it without starting it
	std::cout << "Testing the timeBeforeStart error" << std::endl;
	try{
		std::cout << timer.getStr() << '\n';
		std::cout << "Test failed" << std::endl;
	}
	catch(mee::Stopwatch::invalidTimeResolution){
		std::cout << "There was an invalid time resolution\n";
		std::cout << "Test failed" << std::endl;
	}
	catch(mee::Stopwatch::timeBeforeStart){
		std::cout << "Tried to get time before the stopwatch was started\n";
		std::cout << "Test successful" << std::endl;
	}
	catch(mee::Stopwatch::stopBeforeStart){
		std::cout << "Tried to stop the stopwatch before it was started\n";
		std::cout << "Test failed" << std::endl;
	}

	//Use it correctly
	std::cout << "Using the class correctly" << std::endl;
	try{
		timer.start();
		for(int cnt = 0;cnt < 1000000;++cnt){
			int num = cnt;
			cnt = num;
		}
		timer.stop();
		std::cout << "It took " << timer.getStr() << " to complete this loop\n";
		std::cout << "It took " << timer << " to complete this loop\n";
		std::cout << "Test successful" << std::endl;
	}
	catch(mee::Stopwatch::invalidTimeResolution){
		std::cout << "There was an invalid time resolution\n";
		std::cout << "Test failed" << std::endl;
	}
	catch(mee::Stopwatch::timeBeforeStart){
		std::cout << "Tried to get time before the stopwatch was started\n";
		std::cout << "Test failed" << std::endl;
	}
	catch(mee::Stopwatch::stopBeforeStart){
		std::cout << "Tried to stop the stopwatch before it was started\n";
		std::cout << "Test failed" << std::endl;
	}

	//Test string function
	bool failedTimeResolutionTests = false;
	std::string results = mee::Stopwatch::getStr(1.0);
	if(results != "1.000 nanoseconds"){
		failedTimeResolutionTests = true;
		std::cout << "Failed the nanosecond test" << std::endl;
	}
	results = mee::Stopwatch::getStr(1.0e3);
	if(results != "1.000 microseconds"){
		failedTimeResolutionTests = true;
		std::cout << "Failed the microsecond test" << std::endl;
	}
	results = mee::Stopwatch::getStr(1.0e6);
	if(results != "1.000 milliseconds"){
		failedTimeResolutionTests = true;
		std::cout << "Failed the millisecond test" << std::endl;
	}
	results = mee::Stopwatch::getStr(1.0e9);
	if(results != "1.000 seconds"){
		failedTimeResolutionTests = true;
		std::cout << "Failed the second test" << std::endl;
	}
	results = mee::Stopwatch::getStr(1.0e12);
	if(results != "16.667 minutes"){
		failedTimeResolutionTests = true;
		std::cout << "Failed the minute test" << std::endl;
	}
	results = mee::Stopwatch::getStr(1.0e13);
	if(results != "2.778 hours"){
		failedTimeResolutionTests = true;
		std::cout << "Failed the hour test" << std::endl;
	}
	if(!failedTimeResolutionTests){
		std::cout << "Passed all string resolution tests" << std::endl;
	}

	return 0;
}

/* Results:
Testing the stopBeforeStart error
Tried to stop the stopwatch before it was started
Test successful
Testing the timeBeforeStart error
Tried to get time before the stopwatch was started
Test successful
Using the class correctly
It took 1.855 milliseconds to complete this loop
Test successful
*/
