//myClasses/test/mee/testVectorAlgorithms.cpp
//Matthew Ellison
// Created: 07-02-21
//Modified: 07-02-21
//This file contains tests for the functions in my vetorAlgorithms library
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <iostream>
#include <limits.h>
#include <random>
#include <vector>
#include "mee/numberAlgorithms.hpp"
#include "mee/Stopwatch.hpp"
#include "mee/vectorAlgorithms.hpp"


//Defines a type for functions so there can be an array of functions
typedef bool (*boolFn)();


//Prototypes for test functions
bool testGetSum();
bool testGetProduct();
bool testIsFound();
bool testBubbleSort();
bool testQuickSort();
bool testSearch();
bool testFindMin();
bool testFindMax();
bool testPrintVector();


int main(){
	mee::Stopwatch timer;
	bool passedTest = false;
	bool allPassed = true;
	std::vector<boolFn> functions {testGetSum, testGetProduct, testIsFound, testBubbleSort, testQuickSort, testSearch,
			testFindMin, testFindMax, testPrintVector};
	std::vector<std::string> names { "getSum", "getProduct", "isFound", "bubbleSort", "quickSort", "search",
			"findMin", "findMax", "printVector"};

	//Start doing tests and print out the results of each
	for(size_t cnt = 0;cnt < functions.size();++cnt){
		timer.start();
		passedTest = functions[cnt]();
		timer.stop();
		if(passedTest){
			std::cout << "Function " << names[cnt] << "() passed the test\n";
		}
		else{
			std::cout << "Function " << names[cnt] << "() failed the test\n";
			allPassed = false;
		}
		std::cout << "The test took " << timer.getStr() << "\n\n" << std::endl;
	}

	if(allPassed){
		return 0;
	}
	else{
		return 1;
	}
}

bool testGetSum(){
	uint64_t correctAnswer = 1060;
	uint64_t topNum = 100;
	uint64_t answer = mee::getSum(mee::getPrimes(topNum));
	if(correctAnswer != answer){
		return false;
	}

	//If the false was not triggered it must have passed all tests
	return true;
}

bool testGetProduct(){
	uint64_t correctAnswer = 0;
	std::vector<uint64_t> numbers = {};
	uint64_t answer = mee::getProduct(numbers);
	if(correctAnswer != answer){
		return false;
	}

	correctAnswer = 57600;
	numbers = {2, 2, 3, 3, 4, 4, 100};
	answer = mee::getProduct(numbers);
	if(correctAnswer != answer){
		return false;
	}

	//If a false was not triggered it must have passed all tests
	return true;
}

bool testIsFound(){
	bool correctAnswer = true;
	std::vector<uint64_t> testVector = mee::getPrimes((uint64_t)100);
	uint64_t searchFor = 79;
	bool answer = mee::isFound(testVector, searchFor);
	if(answer != correctAnswer){
		std::cout << "isFound() is failed at test 1" << std::endl;
		return false;
	}

	searchFor = 97;
	answer = mee::isFound(testVector, searchFor);
	if(answer != correctAnswer){
		std::cout << "isFound() is failed at test 2" << std::endl;
		return false;
	}

	searchFor = 88;
	correctAnswer = false;
	answer = mee::isFound(testVector, searchFor);
	if(answer != correctAnswer){
		std::cout << "isFound() is failed at test 3" << std::endl;
		return false;
	}

	//If the false was not triggered it must have passed all tests
	return true;
}

bool testBubbleSort(){
	unsigned int NUM_TO_GENERATE = 10000;
	std::default_random_engine generator(std::random_device{}());
	std::uniform_int_distribution<int> dist(1, INT_MAX);
	std::vector<int> nums;

	//Run through the appropriate number of numbers to generate and add them to the vector
	for(unsigned int cnt = 0;cnt < NUM_TO_GENERATE;++cnt){
		nums.push_back(dist(generator));
	}

	//Sort the numbers with my algorithm
	mee::bubbleSort(nums);

	//Make sure each number is not < the one in behind it
	for(size_t cnt = 1;cnt < nums.size();++cnt){
		if(nums.at(cnt) < nums.at(cnt - 1)){
			return false;
			std::cout << "nums.size() " << nums.size() << "\ncnt " << cnt << std::endl;
		}
	}

	//If the false was not triggered then everything must have been sorted correctly
	return true;
}

bool testQuickSort(){
	unsigned int NUM_TO_GENERATE = 10000;
	std::default_random_engine generator(std::random_device{}());
	std::uniform_int_distribution<int> dist(1, INT_MAX);
	std::vector<int> nums;

	//Run through the appropriate number of numbers to generate and add them to the vector
	for(unsigned int cnt = 0;cnt < NUM_TO_GENERATE;++cnt){
		nums.push_back(dist(generator));
	}

	//Sort the numbers with my algorithm
	mee::quickSort(nums);

	//Make sure each number is not < the one in behind it
	for(size_t cnt = 1;cnt < nums.size();++cnt){
		if(nums.at(cnt) < nums.at(cnt - 1)){
			return false;
		}
	}

	//If the false was not triggered then everything must have been sorted correctly
	return true;
}

bool testSearch(){
	int64_t found = -1;
	//Create a vector of numbers
	std::vector<int> nums {1, 20, 3, 40, 5, 60, 7, 80, 9};
	//Search for one that is in the vector
	found = mee::search(nums, 1);
	if(found != 0){	//This number is in the vector, if it was not found in the vector there is a problem
		return false;
	}

	//Search for another that is in the vector
	found = mee::search(nums, 9);
	if(found != 8){	//This number is in the vector, if it was not found in the vector there is a problem
		return false;
	}

	//Search for another that is in the vector
	found = mee::search(nums, 60);
	if(found != 5){	//This number is in the vector, if it was not found in the vector there is a problem
		return false;
	}

	//Search for a number that is not in the vector
	found = mee::search(nums, 6);
	if(found != -1){
		return false;
	}

	//If it didn't trigger a false then everything went through correctly
	return true;
}

bool testFindMin(){
	std::vector<int> arr {1, 2, 3, 4, 5, 6, 7, 8, 9};
	int answer = 0;

	//Test first
	answer = mee::findMin(arr);
	if(answer != 1){
		return false;
	}

	//Test last
	answer = 0;
	arr = {9, 8, 7, 6, 5, 4, 3, 2, 1};
	answer = mee::findMin(arr);
	if(answer != 1){
		return false;
	}

	//Test middle
	answer = 0;
	arr = {9, 5, 3, 6, 8, 1, 7, 2, 4};
	answer = mee::findMin(arr);
	if(answer != 1){
		return false;
	}

	//Test negative
	answer = 0;
	arr = {-1, -2, -3, -4, -5, -6, -7, -8, -9};
	answer = mee::findMin(arr);
	if(answer != -9){
		return false;
	}

	//If it hasn't failed a test then return true for passing all the tests
	return true;
}

bool testFindMax(){
	std::vector<int> arr {1, 2, 3, 4, 5, 6, 7, 8, 9};
	int answer = 0;

	//Test last
	answer = mee::findMax(arr);
	if(answer != 9){
		return false;
	}

	//Test first
	answer = 0;
	arr = {9, 8, 7, 6, 5, 4, 3, 2, 1};
	answer = mee::findMax(arr);
	if(answer != 9){
		return false;
	}

	//Test middle
	answer = 0;
	arr = {9, 5, 3, 6, 8, 1, 7, 2, 4};
	answer = mee::findMax(arr);
	if(answer != 9){
		return false;
	}

	//Test negative
	answer = 0;
	arr = {-1, -2, -3, -4, -5, -6, -7, -8, -9};
	answer = mee::findMax(arr);
	if(answer != -1){
		return false;
	}

	//If it hasn't failed a test then return true for passing all the tests
	return true;
}

bool testPrintVector(){
	//Test 1
	std::vector<int> nums;
	std::string correctAnswer = "[]";
	std::string answer = mee::printVector(nums);
	if(correctAnswer != answer){
		return false;
	}
	//Test 2
	nums = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	correctAnswer = "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]";
	answer = mee::printVector(nums);
	if(correctAnswer != answer){
		return false;
	}
	//Test 3
	nums = {-3, -2, -1, 0, 1, 2, 3};
	correctAnswer = "[-3, -2, -1, 0, 1, 2, 3]";
	answer = mee::printVector(nums);
	if(correctAnswer != answer){
		return false;
	}

	//Test 4
	std::vector<std::string> strings = {"A", "B", "C"};
	correctAnswer = "[A, B, C]";
	answer = mee::printVector(strings);
	if(correctAnswer != answer){
		return false;
	}
	//Test 5
	strings = {"abc", "def", "ghi"};
	correctAnswer = "[abc, def, ghi]";
	answer = mee::printVector(strings);
	if(correctAnswer != answer){
		return false;
	}

	//If it hasn't failed a test then return true for passing all the tests
	return true;
}
