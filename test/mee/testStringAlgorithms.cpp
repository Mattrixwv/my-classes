//myClasses/test/mee/testStringAlgorithms.cpp
//Matthew Ellison
// Created: 07-02-21
//Modified: 07-02-21
//This file contains tests for the functions in my stringAlgorithms library
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include "mee/Stopwatch.hpp"
#include "mee/stringAlgorithms.hpp"
#include "mee/vectorAlgorithms.hpp"


//Defines a type for functions so there can be an array of functions
typedef bool (*boolFn)();


//Prototypes for test functions
bool testGetPermutations();
bool testFindNumOccurrence();
bool testSplit();
bool testIsPalindrome();


int main(){
	mee::Stopwatch timer;
	bool passedTest = false;
	bool allPassed = true;
	std::vector<boolFn> functions { testGetPermutations, testFindNumOccurrence, testSplit, testIsPalindrome};
	std::vector<std::string> names { "getPermutations", "findNumOccurrence", "split", "isPalindrome"};

	//Start doing tests and print out the results of each
	for(size_t cnt = 0;cnt < functions.size();++cnt){
		timer.start();
		passedTest = functions[cnt]();
		timer.stop();
		if(passedTest){
			std::cout << "Function " << names[cnt] << "() passed the test\n";
		}
		else{
			std::cout << "Function " << names[cnt] << "() failed the test\n";
			allPassed = false;
		}
		std::cout << "The test took " << timer.getStr() << "\n\n" << std::endl;
	}

	if(allPassed){
		return 0;
	}
	else{
		return 1;
	}
}

bool testGetPermutations(){
	std::string permString = "012";
	std::vector<std::string> correctAnswer {"012", "021", "102", "120", "201", "210"};
	std::vector<std::string> answer = mee::getPermutations(permString);
	if(answer != correctAnswer){
		return false;
	}

	//If the false was not triggered it must have passed all tests
	return true;
}

bool testFindNumOccurrence(){
	//Test 1
	std::string testString = "abcdefgdd";
	char testChar = 'a';
	int correctAnswer = 1;
	int answer = mee::findNumOccurrence(testString, testChar);
	if(correctAnswer != answer){
		return false;
	}

	//Test 2
	testChar = 'd';
	correctAnswer = 3;
	answer = mee::findNumOccurrence(testString, testChar);
	if(correctAnswer != answer){
		return false;
	}

	//Test 3
	testChar = 'h';
	correctAnswer = 0;
	answer = mee::findNumOccurrence(testString, testChar);
	if(correctAnswer != answer){
		return false;
	}

	//If it hasn't failed a test then return true for passing all the tests
	return true;
}

bool testSplit(){
	//Test 1
	std::string str = "abc0def";
	std::vector<std::string> correctAnswer = {"abc", "def"};
	std::vector<std::string> answer = mee::split(str, '0');
	if(correctAnswer != answer){
		return false;
	}
	//Test 2
	str = "abc0def";
	correctAnswer = {"abc0def"};
	answer = mee::split(str, '1');
	if(correctAnswer != answer){
		return false;
	}

	//If it hasn't failed a test then return true for passing all the tests
	return true;
}

bool testIsPalindrome(){
	//Test 1
	std::string str = "101";
	bool correctAnswer = true;
	bool answer = mee::isPalindrome(str);
	if(correctAnswer != answer){
		return false;
	}

	//Test 2
	str = "100";
	correctAnswer = false;
	answer = mee::isPalindrome(str);
	if(correctAnswer != answer){
		return false;
	}

	//Test 3
	str = "";
	correctAnswer = true;
	answer = mee::isPalindrome(str);
	if(correctAnswer != answer){
		return false;
	}

	//If it hasn't failed a test then return true for passing all the tests
	return true;
}
